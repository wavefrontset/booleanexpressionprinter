package net.wavefrontset.print;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PossibleBooleanValues implements Iterator<List<Boolean>>, Iterable<List<Boolean>> {

    private final int numberOfVars;
    private List<Boolean> currentValues;

    public PossibleBooleanValues(int numberOfVars) {
        this.numberOfVars = numberOfVars;
    }

    @Override
    public boolean hasNext() {
        return currentValues == null || currentValues.stream().anyMatch(b -> b);
    }

    @Override
    public List<Boolean> next() {
        if (currentValues == null)
            currentValues = new ArrayList<>(numberOfVars);
        removeTrailingFalseValuesAndFlipLastTrueValue();
        fillUpWithTrueValues();
        return currentValues;
    }

    private void removeTrailingFalseValuesAndFlipLastTrueValue() {
        for (int i = currentValues.size()-1; i >= 0; i--) {
            if (currentValues.remove(i)) {
                currentValues.add(false);
                break;
            }
        }
    }

    private void fillUpWithTrueValues() {
        while (currentValues.size() < numberOfVars)
            currentValues.add(true);
    }

    @Override
    public Iterator<List<Boolean>> iterator() {
        return this;
    }
}
