package net.wavefrontset.print;

import net.wavefrontset.interfaces.BooleanExpression;

import java.util.List;
import java.util.stream.IntStream;

public class BooleanExpressionPrinter {

    private static final String HYPHENS = "------";
    private BooleanExpression expression;
    
    public BooleanExpressionPrinter(BooleanExpression expression) {
        this.expression = expression;
    }

    public String printTruthTable() {
        Iterable<List<Boolean>> possibleValues = new PossibleBooleanValues(expression.getNumberOfVariables());
        StringBuilder truthTable = new StringBuilder(printHeadline());
        possibleValues.forEach(value -> truthTable.append(printLineWith(value)));
        return truthTable.toString();
    }

    private String printHeadline() {
        StringBuilder headline = new StringBuilder();
        getExpressionNumbersStream().forEach(i -> headline.append("p").append(i).append("    "));
        headline.append(expression).append("\n");
        getExpressionNumbersStream().forEach(i -> headline.append(HYPHENS));
        IntStream.range(0, expression.toString().length())
                 .forEach(i -> headline.append("-"));
        headline.append("\n");
        return headline.toString();
    }

    private IntStream getExpressionNumbersStream() {
        return IntStream.range(0, expression.getNumberOfVariables());
    }

    private String printLineWith(List<Boolean> values) {
        StringBuilder currentLine = new StringBuilder();
        values.forEach(b -> currentLine.append(String.format("%1$-6s", b)));
        currentLine.append(String.format("   %1$-6s", expression.evaluateOn(values))).append("\n");
        return currentLine.toString();
    }

}
