package net.wavefrontset.test;

import net.wavefrontset.interfaces.BooleanExpression;
import net.wavefrontset.print.BooleanExpressionPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BooleanExpressionPrinterTest {

    private BooleanExpressionPrinter printerForThreeVars;

    @BeforeEach
    void setUp() {
        printerForThreeVars = new BooleanExpressionPrinter(new BooleanExpression() {
            @Override
            public boolean evaluateOn(List<Boolean> truthValues) {
                return truthValues.get(0) && truthValues.get(1) || truthValues.get(2);
            }

            @Override
            public int getNumberOfVariables() {
                return 3;
            }

            @Override
            public String toString() {
                return "p0 && p1 || p2";
            }
        });
    }

    @Test
    void printTruthTable() {
        String expected = "p0    p1    p2    p0 && p1 || p2\n" +
                "--------------------------------\n" +
                "true  true  true     true  \n" +
                "true  true  false    true  \n" +
                "true  false true     true  \n" +
                "true  false false    false \n" +
                "false true  true     true  \n" +
                "false true  false    false \n" +
                "false false true     true  \n" +
                "false false false    false \n";
        assertEquals(printerForThreeVars.printTruthTable(), expected);
    }

}