package net.wavefrontset.test;

import net.wavefrontset.print.PossibleBooleanValues;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PossibleBooleanValuesTest {
    @Test
    void initWith0NumberOfVarsShouldYieldOnePossibleValue() {
        PossibleBooleanValues valuesWith0 = new PossibleBooleanValues(0);
        assertTrue(valuesWith0.hasNext());
        assertTrue(valuesWith0.hasNext());
        assertTrue(valuesWith0.next().isEmpty());
        assertTrue(!valuesWith0.hasNext());
    }

    @Test
    void initWith1NumberOfVarsShouldYield2PossibleValues() {
        PossibleBooleanValues valuesWith1 = new PossibleBooleanValues(1);
        assertTrue(valuesWith1.hasNext());
        List<Boolean> values = valuesWith1.next();
        assertEquals(values.size(), 1);
        assertTrue(values.get(0));
        assertTrue(valuesWith1.hasNext());
        values = valuesWith1.next();
        assertEquals(values.size(), 1);
        assertTrue(!values.get(0));
        assertTrue(!valuesWith1.hasNext());
    }

    @Test
    void initWith4NumbersOfVarsShouldYield16PossibleValues() {
        PossibleBooleanValues valuesWith4 = new PossibleBooleanValues(4);
        int counterOfPossibleValues = 0;
        while (valuesWith4.hasNext()) {
            counterOfPossibleValues++;
            valuesWith4.next();
        }
        assertTrue(counterOfPossibleValues == 16);
    }

    @Test
    void initWith10NumbersOfVarsShouldYield1024PossibleValues() {
        PossibleBooleanValues valuesWith1024 = new PossibleBooleanValues(10);
        int counterOfPossibleValues = 0;
        while (valuesWith1024.hasNext()) {
            counterOfPossibleValues++;
            valuesWith1024.next();
        }
        assertTrue(counterOfPossibleValues == 1024);
    }
}