package net.wavefrontset.interfaces;

import java.util.List;

public interface BooleanExpression {

    boolean evaluateOn(List<Boolean> truthValues);

    int getNumberOfVariables();
}
